import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;

public class PriceUpdaterTest {

    private static Collection<Price> oldPrices,newPrices;

    @Before
    public void setPrices(){
        newPrices = new ArrayList<>();
        oldPrices = new ArrayList<>();
    }

    @Test
    public void mergePriceTest() throws Exception {
        oldPrices.add(new Price(1, "122856", 1, 1,LocalDateTime.of(2013, Month.JANUARY,1,0,0), LocalDateTime.of(2013,Month.JANUARY,31,23,59,59), 11000));
        oldPrices.add(new Price(2, "122856", 2, 1, LocalDateTime.of(2013,Month.JANUARY,10,0,0), LocalDateTime.of(2013, Month.JANUARY,20,23,59,59), 99000));
        oldPrices.add(new Price(3, "6654", 1, 2, LocalDateTime.of(2013,Month.JANUARY,1,0,0), LocalDateTime.of(2013, Month.JANUARY,31,0,0), 5000));

        newPrices.add(new Price(4, "122856", 1, 1, LocalDateTime.of(2013,Month.JANUARY,20,0,0), LocalDateTime.of(2013, Month.FEBRUARY,20,23,59,59), 11000));
        newPrices.add(new Price(5, "122856", 2, 1, LocalDateTime.of(2013,Month.JANUARY,15,0,0), LocalDateTime.of(2013, Month.JANUARY,25,23,59,59), 92000));
        newPrices.add(new Price(6, "6654", 1, 2, LocalDateTime.of(2013,Month.JANUARY,12,0,0), LocalDateTime.of(2013, Month.JANUARY,13,0,0), 4000));

        Collection<Price> prices = new PriceUpdater().mergePrice(oldPrices, newPrices);

        assertEquals("[id:2, product_code:122856, number:2, depart:1, dateBegin:2013-01-10T00:00, dateEnd:2013-01-15T00:00, price:99000\n" +
                ", id:5, product_code:122856, number:2, depart:1, dateBegin:2013-01-15T00:00, dateEnd:2013-01-25T23:59:59, price:92000\n" +
                ", id:3, product_code:6654, number:1, depart:2, dateBegin:2013-01-01T00:00, dateEnd:2013-01-12T00:00, price:5000\n" +
                ", id:6, product_code:6654, number:1, depart:2, dateBegin:2013-01-12T00:00, dateEnd:2013-01-13T00:00, price:4000\n" +
                ", id:7, product_code:6654, number:1, depart:2, dateBegin:2013-01-13T00:00, dateEnd:2013-01-31T00:00, price:5000\n" +
                ", id:1, product_code:122856, number:1, depart:1, dateBegin:2013-01-01T00:00, dateEnd:2013-01-20T00:00, price:11000\n" +
                ", id:4, product_code:122856, number:1, depart:1, dateBegin:2013-01-20T00:00, dateEnd:2013-02-20T23:59:59, price:11000\n" +
                "]", prices.toString());
    }


    @Test
    public void mergePriceTest2() throws Exception{
        oldPrices.add(new Price(1, "122856", 1, 1,LocalDateTime.of(2013, Month.JANUARY,1,0,0), LocalDateTime.of(2013,Month.JANUARY,31,23,59,59), 11000));
        newPrices.add(new Price(4, "122856", 1, 1, LocalDateTime.of(2013,Month.JANUARY,12,0,0), LocalDateTime.of(2013, Month.JANUARY,20,23,59,59), 15000));

        Collection<Price> prices = new PriceUpdater().mergePrice(oldPrices, newPrices);

        assertEquals("[id:1, product_code:122856, number:1, depart:1, dateBegin:2013-01-01T00:00, dateEnd:2013-01-12T00:00, price:11000\n" +
                ", id:4, product_code:122856, number:1, depart:1, dateBegin:2013-01-12T00:00, dateEnd:2013-01-20T23:59:59, price:15000\n" +
                ", id:5, product_code:122856, number:1, depart:1, dateBegin:2013-01-20T23:59:59, dateEnd:2013-01-31T23:59:59, price:11000\n" +
                "]",prices.toString());
    }


    @Test
    public void updatePricesSetTest() throws Exception{

        Price newPrice = new Price(1, "122856", 1, 1, LocalDateTime.of(2013, Month.JANUARY, 1, 0, 0), LocalDateTime.of(2013, Month.JANUARY, 31, 23, 59, 59), 11000);
        Price oldPrice = new Price(4, "122856", 1, 1, LocalDateTime.of(2013,Month.JANUARY,12,0,0), LocalDateTime.of(2013, Month.JANUARY,20,23,59,59), 15000);
        PriceUpdater priceUpdater = new PriceUpdater();
        Index newIndex = new Index(newPrice.getProduct_code(), newPrice.getNumber(), newPrice.getDepart());
        priceUpdater.addToIndexMap(newIndex,newPrice);
        priceUpdater.addToIndexMap(new Index(oldPrice.getProduct_code(),oldPrice.getNumber(),oldPrice.getDepart()),oldPrice);
        boolean test = priceUpdater.updatePricesSet(newPrice, priceUpdater.indexPriceMap.get(newIndex), new TreeSet<>());
        assertEquals(false,test);
    }

}