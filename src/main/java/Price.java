import java.time.LocalDateTime;
import java.util.Date;

public class Price {
    private static long id_max = Long.MIN_VALUE;
    private long id; // идентификатор в БД
    private String product_code; // код товара
    private int number; // номер цены
    private int depart; // номер отдела
    private LocalDateTime begin; // начало действия
    private LocalDateTime end; // конец действия
    private long value; // значение цены в копейках
    private Index index; // индекс цены, строящийся на основе number, product_code, depart

    Price(long id, String product_code, int number, int depart, LocalDateTime begin, LocalDateTime end, long value) {
        this.id = id;
        this.product_code = product_code;
        this.number = number;
        this.depart = depart;
        this.begin = begin;
        this.end = end;
        this.value = value;
        if(id_max < id)
            setId_max(id);
    }

    private void setIndex(){
        this.index = new Index(product_code,number,depart);
    }

    Index getIndex() {
        if (index == null)
            setIndex();
        return index;
    }

    LocalDateTime getBegin() {
        return begin;
    }

    void setBegin(LocalDateTime begin) {
        this.begin = begin;
    }

    void setEnd(LocalDateTime end) {
        this.end = end;
    }

    LocalDateTime getEnd() {
        return end;
    }

    static long get_nex_Id() {
        id_max++;
        return id_max;
    }

    String getProduct_code() {
        return product_code;
    }

    int getNumber() {
        return number;
    }

    int getDepart() {
        return depart;
    }

    long getValue() {
        return value;
    }

    private static void setId_max(long id_max) {
        Price.id_max = id_max;
    }

    @Override
    public String toString() {
        return "id:" +id + ", product_code:" + product_code + ", number:" + number + ", depart:" + depart +   ", dateBegin:"+ this.getBegin() + ", dateEnd:" + this.getEnd() + ", price:" + this.getValue()+
                "\n";
    }

}
