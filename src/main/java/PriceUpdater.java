import java.util.*;

class PriceUpdater {

    HashMap<Index, TreeSet<Price>> indexPriceMap = new HashMap<>();


    Collection<Price> mergePrice(Collection<Price> oldPrices, Collection<Price> newPrices){

        for (Price oldPrice : oldPrices) {
            Index index = oldPrice.getIndex();
            addToIndexMap(index,oldPrice);
        }
        for (Price newPrice : newPrices) {
            Index index = newPrice.getIndex();
            if (indexPriceMap.containsKey(index)) {
                TreeSet<Price> pricesSet = indexPriceMap.get(index);
                TreeSet<Price> resultSet = new TreeSet<>(Comparator.comparing(Price::getBegin));
                Price floor =  pricesSet.floor(newPrice);
                SortedSet<Price> tailSet;
                if (floor == null){
                    tailSet = pricesSet;
                }
                else {
                    boolean includeFloorToHead=false;
                    if (floor.getEnd().isBefore(newPrice.getBegin())){
                        includeFloorToHead = true;
                    }
                    resultSet.addAll(pricesSet.headSet(floor, includeFloorToHead));
                    tailSet = pricesSet.tailSet(floor, !includeFloorToHead);
                }
                boolean isNotFinished;
                do {
                    isNotFinished = updatePricesSet(newPrice,tailSet,resultSet);
                } while (!isNotFinished&&tailSet.size()!=0);
                resultSet.add(newPrice);
                resultSet.addAll(tailSet);
                indexPriceMap.put(index,resultSet);
            } else {
                addToIndexMap(index,newPrice);
            }
        }
        Collection<Price> result = new ArrayList<>();
        indexPriceMap.values().forEach(result::addAll);

        return result;
    }

    boolean updatePricesSet(Price newPrice, SortedSet<Price> tailSet, SortedSet<Price> resulSet) {
        Price firstPrice = tailSet.first();
        //начало новой цены меньше или равно началу минимальной цены в сете
        if (newPrice.getBegin().isBefore(firstPrice.getBegin())||newPrice.getBegin().isEqual(firstPrice.getBegin())) {
            // конец новой цены меньше или равен начала минимальной цены
            if ((newPrice.getEnd().isBefore(firstPrice.getBegin()))) {
                return true;
            }
            // конец новой цены до конца минимальной цены
            else if (newPrice.getEnd().isBefore(firstPrice.getEnd())) {
                firstPrice.setBegin(newPrice.getEnd());
                return true;
            } // пришедшая цена полностью перекрывает минимальную цену
            else {
                tailSet.remove(firstPrice);
                return false;
            }
            // начало новой цены после начала минимальной цены
        }else {

            // конец новой цены до конца минимальной цены
            if (newPrice.getEnd().isBefore(firstPrice.getEnd())){
                tailSet.add(new Price(Price.get_nex_Id(),firstPrice.getProduct_code(),firstPrice.getNumber(), firstPrice.getDepart(),newPrice.getEnd(),firstPrice.getEnd(),firstPrice.getValue()));
                firstPrice.setEnd(newPrice.getBegin());
                return true;
            }// конец новой цены после конца минимальной цены
            else{
                firstPrice.setEnd(newPrice.getBegin());
                resulSet.add(firstPrice);
                tailSet.remove(firstPrice);
                return false;
            }

        }
    }

    void addToIndexMap(Index index, Price price) {
        if (!indexPriceMap.containsKey(index))
            indexPriceMap.put(index,(new TreeSet<>(Comparator.comparing(Price::getBegin))));
        indexPriceMap.get(index).add(price);
    }

}
