
public final class Index{
    private String product_code; // код товара
    private int number; // номер цены
    private int depart; // номер отдела

    Index(String product_code, int number, int depart) {
        this.product_code = product_code;
        this.number = number;
        this.depart = depart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Index index = (Index) o;

        return number == index.number && depart == index.depart && product_code.equals(index.product_code);
    }

    @Override
    public int hashCode() {
        int result = product_code.hashCode();
        result = 31 * result + number;
        result = 31 * result + depart;
        return result;
    }
}
